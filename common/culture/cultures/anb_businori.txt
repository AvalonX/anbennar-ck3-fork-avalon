﻿businori_group = {
	graphical_cultures = {
		frankish_group_coa_gfx
		western_coa_gfx
		western_building_gfx
		western_clothing_gfx
		western_unit_gfx
		dde_hre_clothing_gfx
	}
	mercenary_names = {
	}

	busilari = {
		
		color = { 211 85 4 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {
			Abrahil Abram Adalbald Adalbert Adalgari Adalgrim Aegidius Ageric Agilbert Agiulf Ailbert Albric Aldedramn
			Andica Ansovald Arcambald Aregisel Arnegisel Arnulf Ascaric Audovald Austregisel Autbert Autgari Autgeri
			Avremar Badegisel Balduin Barnard Berald Bernard Berneri Bero Berold Berthefried Bertlin Bertram
			Bertrand Burchard Karloman Ceslin Chararic Charibert Childebert Childeric Chilperic
			Chlodomer Chlothar Chramnesind Clodio Clodion Clovis Creat Dagaric Dagobert Drogo Eberulf Ebregisel Engilbert
			Euric Everard Faroard Faroin Feremund Feroard Foroen Frobert Frotari Frothard Frothari Frotlaic Fulcari Fulcrad
			Galteri Gararic Garivald Gaucelm Gaudulf Gaujoin Gausbert Gausbold Gautmar Gauzbert Gedalbert Gedalca Genobaud Gerbert
			Gerhard Gerold Gislari Gislevert Gocelm Godalbert Godomar Gozhelm Grimald Guadulf Gualtari Gualter Guillabert Guitard Gundobad
			Gunthar Guntram Haldemar Hartmut Hildebald Hildebold Hildegaud Hildevold Hildoin Hucbert Hugbert Imnachar Ingalbert
			Ingomer Karl Lambert Lantbert Leudast Lothar Magnachar Magneric Mainard Mallobaudes Marachar Marcomer
			Marell Martin Maurifi Meginhard Merogais Merovech Munderic Niebelung Odelric Odolric Otbert Otgeri Otker Pepin Pharamond
			Pippin Radulf Ragambald Ragena Ragenard Raginari Ragnachar Ragnald Ragno Raimbold Rainald Ramnulf Rathar
			Raynold Reginari Ricchar Rignomer Roland Robert Rotbert Segoin Seguin Sicbald Sichar Sicland Sicleard Siclevold
			Sigebald Sigebert Sigeric Sigismund Sigobert Sinop Sunnegisil Sunno Tancrad Tancred Tassilo Teotbert Tetbert Teutbald Teutbert
			Theoderic Theoric Theudebald Theudemeres Theuderic Theudoald Theutbald Trutgaud Vuitard Vulfari Vulframn Vulvari
			Waltgaud Werinbert Wilbert Willichar Wolfari
		}
		female_names = {
			Adalgardis Adallinda Adaltrude Adaluildis Adelaidis Airsenda Albofleda Albrada Alda Aldegonde Aliberta Alitrudis Ansegudis Ansegundis
			Anstrude Arsindis Audofleda Audovera Austreberta Austrechild Balthild Begga Beretrude Bernegildis Bertenildis Berthefled Berthefried
			Berthegund Berthildis Bertilla Bertrada Bladovildis Brunhild Burgundofara Celsa Celsovildis Cesaria Chlodosind Chlothsinda Clotild Creada
			Dagena Eldesendis Ermengardis Ermengildis Ermensindis Eustadiola Faileuba Faregildis Fastrada Framberta Fredegunde Frolaica Frotberga
			Frotlildis Frotlina Galswinth Gaudildis Gautlindis Genovefa Gersvinda Gertrude Gisela Glodesind Goiswinth Gotberga Gundrada Halderudis
			Harildis Hildegarde Hildegardis Hildesendis Hiltrude Illegardis Ingitrude Ingohildis Ingunde Itta Landina Lanthechilde Lantsida Leubast Leubovera
			Leutberga Leutgardis Liutgarde Madelgarde Magnatrude Marcatrude Marcovefa Martinga Monegund Morberga Radegund Rictrude Rigunth
			Rosamund Rothaide Rotrude Ruothilde Rusticula Sadalberga Siclehildis Sigalsis Theodelinda Theoderada Ultrogotha Vuldretrada Waltrude
		}

		dynasty_of_location_prefix = "dynnp_sil"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			100 = mediterranean
		}
	}

	#old type
	businori = {
		
		color = { 111 85 4 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {

			#Vanilla - from Suebi, the proto-Arannese whatever the fuck they're called are Visigoth
			Adeqisio Adoric Agimadus Agriulfum Alamiro Algaster Anila Ansuario Argiovitus Ariamir Arosinda Audeca
			Badamundus Baldemarius Balderedo Baltarius Baudemiro Becilla Berteiro Bonesindus Boniza Cagildo Cartemiro
			Chararici Commundus Crodemiro Dagaredus Daildus Damiro Docemiro Donazarius Donegildus Dulcemirus Eboric
			Egenio Eimiro Eirigu Eisindus Eldegotus Eldesindus Elperico Engomiro Ensaldus Ermaric Evosindo Fagildus
			Felmiro Fralenko Framuldo Francolino Fredoaldus Froatingus Fromildus Frumeiro Gardingus Gaudesindo Gomedei
			Gomulfus Gresumarus Gudesteus Gumarius Gundivaldo Guntato Gunterotis Heremigar Hermeric Hespanisco Hildemir
			Hilderic Hunisco Huniscus Leodegasti Leoderigus Leodulfus Leovaldo Lodemiro Magitus Malaredus Malaric Maldras
			Marcosendus Massilan Miro Moderido Nandaricus Nantildo Neufila Nitigis Notarius Odamiro Odemundus
			Odericus Onemiro Onoricus Osdulfus Osorico Otualdo Pabregildus Pantard Pantinus Pappinus Pederagildu
			Pennio Pipinus Provaredo Quedesendo Quedulfus Quidemiro Quidericus Ragimiro Ragolfus Rakericus Randinus
			Randuarius Ranisclus Rechiar Rechila Refulfo Reirigus Remismund Remisol Rendericus Ricardo Richimund Rodomiro
			Roelindus Romanisco Romarigus Sagatus Saxomero Segomirus Sentarius Sescutus Sisulfus Spanarius Spoderigo Suavarius
			Sunila Teobald Teoderedus Theodemar Todegogia Trasmira Trasoldi Tumtuldo Uittimer Venedario Veremund Viamundus
			Vidraldus Villisendo Vistremiro Zamarius Zendasindo
		}
		female_names = {
			Acilo Ailo Alatrudia Aliverga Aliverta Aniedrudia Ansetrudia Astriverga Bagina Berina Cenabrida Cisilo Dadilo Dagadruida
			Desteilli Donadildi Drusilla Ebragundia Ebrildi Eldegundia Emilo Entrudi Ermedrudia Ermildi Espallo Facalo Fagilo Faquilo
			Fradegundia Framilli Fremosilli Frogildi Froilo Frolio Fronosili Frumildi Frunilo Gelvira Genlo Godoigia Goldrerona Granilo
			Guandilo Gundiverga Guntedrudia Ildiverga Kindiverga Lallina Liuvilo Mansuara Margilli Mirosinda Penefreda Penetrudia
			Ragesenda Randilo Raniverta Recerona Recesenda Recesuinda Rezevera Riquilo Rudilo Sabegoto Senuita Sindiverga
			Sisilli Sisiverta Sitividis Sontrilli Sparuildi Stodildi Suevisco Tanquilli Tederona Tenildi Teodevirga Teodiverta Teodoriga
			Teudileova Texilli Tractuilli Trasavara Trasmira Trastivigia Vergina Viscaverga Visterlo Vistravara Vistrildi Zanina
		}

		dynasty_of_location_prefix = "dynnp_of"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			100 = mediterranean
		}
	}
	
	tefori = {
		
		color = { 207 182 100 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {

			#Vanilla - from Suebi, the proto-Arannese whatever the fuck they're called are Visigoth
			Adeqisio Adoric Agimadus Agriulfum Alamiro Algaster Anila Ansuario Argiovitus Ariamir Arosinda Audeca
			Badamundus Baldemarius Balderedo Baltarius Baudemiro Becilla Berteiro Bonesindus Boniza Cagildo Cartemiro
			Chararici Commundus Crodemiro Dagaredus Daildus Damiro Docemiro Donazarius Donegildus Dulcemirus Eboric
			Egenio Eimiro Eirigu Eisindus Eldegotus Eldesindus Elperico Engomiro Ensaldus Ermaric Evosindo Fagildus
			Felmiro Fralenko Framuldo Francolino Fredoaldus Froatingus Fromildus Frumeiro Gardingus Gaudesindo Gomedei
			Gomulfus Gresumarus Gudesteus Gumarius Gundivaldo Guntato Gunterotis Heremigar Hermeric Hespanisco Hildemir
			Hilderic Hunisco Huniscus Leodegasti Leoderigus Leodulfus Leovaldo Lodemiro Magitus Malaredus Malaric Maldras
			Marcosendus Massilan Miro Moderido Nandaricus Nantildo Neufila Nitigis Notarius Odamiro Odemundus
			Odericus Onemiro Onoricus Osdulfus Osorico Otualdo Pabregildus Pantard Pantinus Pappinus Pederagildu
			Pennio Pipinus Provaredo Quedesendo Quedulfus Quidemiro Quidericus Ragimiro Ragolfus Rakericus Randinus
			Randuarius Ranisclus Rechiar Rechila Refulfo Reirigus Remismund Remisol Rendericus Ricardo Richimund Rodomiro
			Roelindus Romanisco Romarigus Sagatus Saxomero Segomirus Sentarius Sescutus Sisulfus Spanarius Spoderigo Suavarius
			Sunila Teobald Teoderedus Theodemar Todegogia Trasmira Trasoldi Tumtuldo Uittimer Venedario Veremund Viamundus
			Vidraldus Villisendo Vistremiro Zamarius Zendasindo
		}
		female_names = {
			Acilo Ailo Alatrudia Aliverga Aliverta Aniedrudia Ansetrudia Astriverga Bagina Berina Cenabrida Cisilo Dadilo Dagadruida
			Desteilli Donadildi Drusilla Ebragundia Ebrildi Eldegundia Emilo Entrudi Ermedrudia Ermildi Espallo Facalo Fagilo Faquilo
			Fradegundia Framilli Fremosilli Frogildi Froilo Frolio Fronosili Frumildi Frunilo Gelvira Genlo Godoigia Goldrerona Granilo
			Guandilo Gundiverga Guntedrudia Ildiverga Kindiverga Lallina Liuvilo Mansuara Margilli Mirosinda Penefreda Penetrudia
			Ragesenda Randilo Raniverta Recerona Recesenda Recesuinda Rezevera Riquilo Rudilo Sabegoto Senuita Sindiverga
			Sisilli Sisiverta Sitividis Sontrilli Sparuildi Stodildi Suevisco Tanquilli Tederona Tenildi Teodevirga Teodiverta Teodoriga
			Teudileova Texilli Tractuilli Trasavara Trasmira Trastivigia Vergina Viscaverga Visterlo Vistravara Vistrildi Zanina
		}


		dynasty_of_location_prefix = "dynnp_of"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			100 = mediterranean
		}
	}

	#tribal version of ancient Milcorissians
	milcori = {
		
		color = { 91 135 192 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {
			#Vanilla - Georgian
			Adarnase Aghsartan Aleksandre Andronik Anzori Archili Ashot Davit Demetre Demna Erekle Giorgi Grigoli Guaram
			Gurgen Iese Ioane Iveri Kaikhosro Kakhi Koba Levan Luarsab Mamia Merab Mikheil Mukhran Nerse Nikoloz Okhropir Orbeli
			Padla Samoeli Shanshe Simon Stepanoz Sumbat Teimuraz Tornike Ushisha Vache Vakhtang Zurab Zviad

			#Vanilla - Visigothic, for that link with Businor
			Adelfonso Alaricho Alatheo Aloito Amalmondo Aoricho Ardo Diaco Ermengar Gaton Mauregato Odoario Ordonno Petro Radulfo Ricimero Ermengar 

			#Vanilla - more "modern" Polish Arannese
			Aleksander Antoni Bogislav Dervan Drosuk FrantiS_ek Havel Jacenty Jakub Jaromar
			Jaromir Jozef Kahetan Karel Kasimir Kasper Kornel KreS_imir
			Lubomir Lucjan Marek Marian Martin Niklot Patryk Petrus Piotr Radomil Radomir Sambor
			Walenty 
		}
		
		female_names = {
			Ana Baia Bedisa Borena Chiora Chito Dali Dedika Duda Dzabuli Elene Eliso Endzela Gogutsa Gulchara Guliko Gulisa Gulvardi Guranduht Gvantsa Ia Judjuna
			Kekela Ketevan Lamara Lamzira Lela Mariami Mathiko Mthvarisa Mzekhar Natia Nino Rusudan Sanatha Shorena Shukhia Suliko
			Tamari Taplo Tinatini Tsiala Vardo

			Adosinda Argilo Ariberga Cixillo Cruesa Egilona Eldonza Elduara Elvira Ermengardis Flamula Fronidle Gaudiosa Gonterote Gothina Paterna Patruina Ralindis
			Riciberga Ricilo Sibilia Sisenanda Theodora Urraca Winidilda

			#Vanilla - more "modern" Polish Arannese
			Angiezka BoZ_ena Branislava Dagmar Danuta Dobrava Dobroniega Dobroslava Dorota Dragomira Edyta Eva Geira Gertruda
			Gracja Halina Hanna Irena Izabela Jarmila Jolanta Judyta Kamila Katarina Kenna Kornelia Krystyna Ksenia Lidia
			Ludmila Magda Magdalena Marcelina Maria Martyna Matylda Milena
			Raina Regelinda Rycheza Salomea Stefana UrS_ula VA_clava VE_ra Woislava Zofia Zvinislava
		}

		dynasty_of_location_prefix = "dynnp_of"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			100 = milcorissian
		}
	}
}