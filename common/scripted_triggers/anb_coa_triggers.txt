﻿#Triggers to control Coat of Arms weighting

coa_verne_dameria_trigger = {
	OR = {
	primary_title = {
		this = title:k_dameria
	}
	any_liege_or_above = {
		primary_title = {
			this = title:k_dameria
		}
	}
}

coa_damerian_trigger = {
	culture = { has_graphical_culture = damerian_coa_gfx }
}

coa_old_damerian_trigger = {
	culture = { has_graphical_culture = old_damerian_coa_gfx }
}

coa_vernman_trigger = {
	culture = { 
		OR = {
			has_graphical_culture = vernman_coa_gfx
			has_graphical_culture = vernid_coa_gfx
		}
	}
}

coa_gawedi_trigger = {
	culture = { has_graphical_culture = gawedi_coa_gfx }
}
