k_verne = {
	967.2.21 = {	#whoever Arman's dad is, he dies
		holder = 513	#Arman Vernid
	}
	980.6.30 = {
		holder = 48	#Rocair Vernid
	}
}

c_stingport = {
k_verne = {
	967.2.21 = {	#whoever Arman's dad is, he dies
		holder = 513	#Arman Vernid
	}
	980.6.30 = {	#Slaughter at Cronesford, Rocair's dad dies
		holder = 48	#Rocair Vernid
	}
	980.10.12 = {	#Sorncost-Pearlsedge join the war and make base at Stingport. Pearlsedge refuses to give it back!
		holder = 3	#Aron Pearlman
	}
}